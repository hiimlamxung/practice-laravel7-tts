<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="{{ asset('css/MyCss.css') }}">
	<title>Luyện tập về View</title>
</head>
<body>
	@include('practice-view.layout.header')
	<div id="content">
		<h1>Học laravel</h1>
		@yield('NoiDung')
	</div>
	@include('practice-view.layout.footer')
</body>
</html>