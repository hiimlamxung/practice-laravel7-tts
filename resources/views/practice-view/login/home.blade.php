{{--Check xem da dang nhap chua--}}
@if(Auth::check())
	<h1>Đăng nhập thành công</h1>
	@if(isset($data_user))
		{{ "Tên: ".$data_user->name }}<br>
		{{ "email: ".$data_user->email }}
	@endif
	<a href="/logout">Logout</a>
@else
	<h1>Bạn chưa đăng nhập</h1>
@endif