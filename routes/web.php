<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/baiviet/{id}/{categoryId}', function($id, $categoryId){
	return "Đây là bài viết số $id thuộc category $categoryId";
});

Route::get('/sanpham/{id}/{categoryId}', function($id, $categoryId){
	return "Đây là sản phẩm số $id. Thuộc category số $categoryId";
})->name('sanpham.show');

Route::group(['prefix' => 'Backend','namespace' => 'Backend'], function(){
	Route::get('user', 'UserController@index')->name('backend.user');
	Route::get('user/create', 'UserController@create')->name('backend.user.create');
	Route::post('user/store', 'UserController@store')->name('backend.user.store');
	Route::put('user/update', 'UserController@update')->name('backend.user.update');

	Route::get('blog', 'BlogController@index')->name('backend.blog');
	Route::get('product', 'ProductController@index')->name('backend.product');
});


//Route Cookie
Route::get('setCookie', 'Backend\UserController@setCookie');
Route::get('getCookie', 'Backend\UserController@getCookie');

//JSON
Route::get('getJson', 'Backend\UserController@getJson');

//View Share, Tạo biến - biến này view nào cũng dùng đc
View::share('bien', 'Day la gia tri bien');

Route::get('practice-view', 'Backend\UserController@practiceView');


//DATABASE
Route::get('db/get', function() {
    $data = DB::table('users')->select(['id','name','email'])->where('id',1)->get();
    dd($data);
});

//Model - Tạm thời viết logic ngay tại phần Route.
Route::get('model/save', function(){
	$user = new App\User;
	$user->name = "Hoang Lam";
	$user->email = "lam@gmail.com";
	$user->password = "123456";

	$user->save();
});
Route::get('model/query', function(){
	$user = new App\User;
	$info = $user->find(13);
	echo $info->name;
});
Route::get('model/sanpham/save', function(){
	$sanpham = new App\Models\SanPhamModel;
	$sanpham->name = "Panasonic 120inch";
	$sanpham->gia = 2000000;
	$sanpham->id_loaisp = 3;
	$sanpham->save();

});
Route::get('model/loai_sanpham/save', function(){
	$loai_sanpham = new App\Models\LoaiSanPhamModel;
	$loai_sanpham->ten_loaisp = "Tivi";
	$loai_sanpham->save();

});
Route::get('model/sanpham/all', function(){
	$sanpham = new App\Models\SanPhamModel;
	$data = $sanpham->all()->toJson(); // hoac toArray();
	echo $data;
});
Route::get('model/sanpham/name', function(){
	$sanpham = new App\Models\SanPhamModel;
	$data = $sanpham->where('name','Laptop')->get()->toArray();
	echo $data[0]['name'];
});

//Lieen ket bảng bằng Model
Route::get('lienket', function(){
	$data = App\Models\SanPhamModel::find(3)->loaisanpham->toArray();
	echo "<pre>";
	print_r($data);
	echo "</pre>";
});
Route::get('lienket2', function(){
	$data = App\Models\LoaiSanPhamModel::find(1)->sanpham->toArray();
	echo "<pre>";
	print_r($data);
	echo "</pre>";
	
});

//Middleware
Route::get('diem', function(){
	echo "bạn đã có điểm";
})->middleware('MyMiddleware')->name('diem');

Route::get('nhapdiem', function(){
	return view('practice-view.nhapdiem');
})->name('nhapdiem');

Route::get('loi', function(){
	echo "Bạn chưa có điểm";
})->name('loi');


//Auth
Route::get('dangnhap', function(){
	return view('practice-view.login.login');
});
Route::post('login','Backend\AuthController@login')->name('LogintoHome');
Route::get('logout', 'Backend\AuthController@logout');


//Session

	Route::get('session',function(){
		Session::put('KhoaHoc','Laravel');
		echo "Đã đặt session<br>";
		echo Session::get('KhoaHoc');
		if(Session::has('KhoaHoc')){
			echo "<br>Session KhoaHoc có tồn tại";
		}else{
			echo "<br>Session KhoaHoc ko tồn tại";
		}
		
	});

//Pagination
	Route::group(['prefix' => 'Backend','namespace' => 'Backend'], function(){
		Route::get('sanpham', 'SanPhamController@index')->name('sanpham');
	});
