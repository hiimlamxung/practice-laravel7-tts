<?php

use Illuminate\Database\Seeder;

class SanPhamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=13; $i<=25; $i++){
        	DB::table('sanpham')->insert(['name' => 'IPhone'.$i, 'gia' => 8000000, 'id_loaisp' => 1]);
        }
    }
}
