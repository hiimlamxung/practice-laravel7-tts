<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SanPhamSeeder::class);

        // for($i=11; $i<=20; $i++){
        // 	DB::table('users')->insert(['name' => 'lam'.$i, 'email' => 'lam'.$i.'@gmail.com', 'password' => bcrypt('123456')]);
        // }
    }
}
