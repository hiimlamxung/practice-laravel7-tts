<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class AuthController extends Controller
{
    public function login(Request $request)
    {	
    	//Dang nhap bang Auth:attempt
    	$name 		= $request->name;
    	$password	= $request->password;
    	if(Auth::attempt(['name' => $name, 'password' => $password])){
    		//Nếu đăng nhập thành công, hàm attempt trả về true
    		return view('practice-view.login.home',['data_user' => Auth::user()]);
    	}else{
    		return view('practice-view.login.login',['error' => 'Đăng nhập thất bại']);
    	}


  		
    }

    public function logout()
    {
    	Auth::logout();
    	return view('practice-view.login.login');
    }
}
