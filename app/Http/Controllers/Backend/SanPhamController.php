<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SanPhamModel;
class SanPhamController extends Controller
{
    public function index(){
    	$sanpham = SanPhamModel::where('id','<','10')->paginate(2);
    	return view('Backend.sanpham.sanpham',['sanpham' => $sanpham]);
    }
}
