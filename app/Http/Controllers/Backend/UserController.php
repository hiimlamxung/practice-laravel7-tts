<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserController extends Controller
{
    public function index()
    {
    	return "List User";
    }

    public function create()
    {
    	return view('Backend.user.create');
    }

    public function store(Request $request)
    {
        // dd($request->except('name'));

        //Kiem tra file co ton tai ?
        if($request->hasFile('myFile')){
            //Luu file
            $file = $request->file('myFile');
            dd($file->getSize());
            // $file->move(
            //     'img', //noi luu file
            //     'anh1.jpg', //ten file
            // );
              
        }else{
            echo "Chua co file";
        }
    }

    public function update(Request $request)
    {
    	dd(__METHOD__,$request->all()); 
    }

    public function setCookie()
    {
        $response = new Response();
        $response->withCookie(
            'KhoaHoc', //Ten cookie
            'Laravel-KhoaHoc', //Gia tri
            1 //Thoi gian, tinh theo phut
        );
        return $response;
    }

    public function getCookie(Request $request)
    {
        return $request->cookie('KhoaHoc');
    }

    //JSON
    public function getJson()
    {
        $arr = ['PHP','ASP.NET','JAVA'];
        return response()->json($arr);
    }

    public function practiceView()
    {
        $bai = '<i style="color:red;">Số 2</i>';
        $arr = array(
            'bai1' => 'lam',
            'bai2' => 'kien',
            'bai3' => 'khoa',
        );
        return view('practice-view.page.laravel',['bai' => $bai, 'arr' => $arr]);
    }
}
