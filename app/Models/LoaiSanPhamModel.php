<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoaiSanPhamModel extends Model
{
    //
    protected $table = "loai_sanpham";

    public function sanpham()
    {
    	return $this->hasMany('App\Models\SanPhamModel','id_loaisp','id');
    }
}
