<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SanPhamModel extends Model
{
    protected $table = "sanpham"; //Tên bảng ở db

    public function loaisanpham()
    {
    	return $this->belongsTo('App\Models\LoaiSanPhamModel','id_loaisp','id'); //Liên kết 1-1 tới bảng cha. khóa ngoại là id_loai sp. khóa chinh là id.
    }
    
}
