<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Sidebar extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $bienTitle;
    public $email;

    public function __construct($bienTitle, $email)
    {
        $this->bienTitle = $bienTitle;
        $this->email = $email;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.sidebar');
    }

    public function users($user)
    {
        return [
            'Nguyễn Văn A',
            'Nguyễn Văn B',
            'Nguyễn Văn C',
            $user
        ];
    }
}
